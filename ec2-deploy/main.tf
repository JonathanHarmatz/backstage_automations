variable "region" {
  description = "The AWS region to deploy the EC2 instance"
  type        = string
}

variable "instance_name" {
  description = "The name for the EC2 instance"
  type        = string
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "vpc_a" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

    tags = {
    Name = "VPC_backstage_demo"
  }
}
